Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: phosh
Upstream-Contact: Guido Günther <agx@debian.org>
Source: https://source.puri.sm/Librem5/phosh

Files: *
Copyright: 2000 Eazel, Inc.
 2007-2008 Red Hat, Inc.
 2014 Collabora Ltd
 2018-2020 Purism SPC
 2019-2020 Zander Brown <zbrown@gnome.org>
 2019 Alexander Mikhaylenko <exalm7659@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: 2018 Purism SPC
 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
License: GPL-3+

Files: protocol/gamma-control.xml
Copyright: 2015 Giulio camuffo
License: MIT

Files: protocol/idle.xml
Copyright: 2015 Martin Gräßlin
License: LGPL-2.1+

Files: protocol/wlr-foreign-toplevel-management-unstable-v1.xml
Copyright: 2018 Ilia Bozhinov
License: MIT

Files: protocol/wlr-input-inhibitor-unstable-v1.xml
 protocol/wlr-layer-shell-unstable-v1.xml
Copyright: 2017, 2018 Drew DeVault
License: MIT

Files: protocol/wlr-output-management-unstable-v1.xml
 protocol/wlr-output-power-management-unstable-v1.xml
Copyright: 2019 Purism SPC
License: MIT

Files: src/contrib/*
Copyright: 2011 Red Hat, Inc.
 2011 Giovanni Campagna <scampa.giovanni@gmail.com>
 2017 Lubomir Rintel <lkundrak@v3.sk>
License: GPL-2+

Files: src/gtk-list-models/gtkfilterlistmodel.*
 src/gtk-list-models/gtksortlistmodel.*
Copyright: 2018 Benjamin Otte
License: LGPL-2.1+

Files: src/gtk-list-models/gtkrbtree*
Copyright: 2000 Red Hat, Inc., Jonathan Blandford <jrb@redhat.com>
License: LGPL-2+

Files: subprojects/gvc/*
Copyright: 2006-2008 Lennart Poettering
 2008-2009 Red Hat, Inc.
 2008 William Jon McCann
 2008 Sjoerd Simons <sjoerd@luon.net>
 2009 Bastien Nocera
 2011-2012 Conor Curran <conor.curran@canonical.com>
 2012 David Henningsson, Canonical Ltd. <david.henningsson@canonical.com>
License: GPL-2+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: LGPL-2.1+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 2.1 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
